.. index documentation master file, created by
   sphinx-quickstart on Thu Mar 14 14:59:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Initialization
=====================================

In Your app main activity of the main module (e.g "app") of the project

.. code-block:: java

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ..
        // initialize SDK       
		// false if for development environment, true if for production environment		
		GtvSdk.init(this, false,object: GtvConfigCallback{
            override fun onFinish(response: Boolean) {
                super.onFinish(response)
				
                //response: false if config service false, true if config service true     
                Log.i("GTV_Config_Services",response.toString())
            }

        })
        ..
    }    