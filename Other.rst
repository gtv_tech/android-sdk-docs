.. index documentation master file, created by
   sphinx-quickstart on Thu Mar 14 14:59:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Other
=====================================


Get Review
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Use this line to show payment method

.. code-block:: java
	
	// true is review
	// false is not review

	GTVSDK.getInstance().getReview();