.. Setup

Setup
=====================================

Required Information
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Some information we must provided for the partner to use. Please ask for it if you don't see it. They are:

- Android and iOS bundle identifier
- Configuration file gtv.xml
- Firebase google-services.json file
- Firebase GoogleService-Info.plist file
- SDK workflow and server api documentation
- Android keystore and password for it
- Apple provision profile (Adhoc and Production)
- Apple certificate and password for it

Install SDK
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Import the .arr file we sent with the SDK to the project

2. Add the new module from the .arr file to module dependencies of the project (via Project Structure -> Dependencies)

3. Copy google-services.json file to the root folder of the main module (e.g "app") of the project

4. Copy gtv.xml to res/value folder of the main module of the project

5. Add this line to build.gradle of the project

.. code-block:: groovy 
    
    classpath 'com.google.gms:google-services:4.3.3' // required for firebase

6. Add these lines to build.gradle of the main module of the project 

.. code-block:: groovy 
    
    apply plugin: 'com.google.gms.google-services' // required for firebase

.. code-block:: groovy 

    implementation 'com.facebook.android:facebook-login:[5,6)' // required for facebook
    implementation 'com.google.firebase:firebase-analytics:17.4.3'// required for firebase
    implementation 'com.google.firebase:firebase-messaging:20.2.1'// required for firebase push notification
    implementation 'com.android.billingclient:billing:2.1.0' // required iap
    implementation 'com.appsflyer:af-android-sdk:4.8.7@aar' // required for appsflyer
	implementation 'com.google.android.gms:play-services-auth:18.1.0'//gg auth
