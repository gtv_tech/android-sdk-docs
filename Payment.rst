.. index documentation master file, created by
   sphinx-quickstart on Thu Mar 14 14:59:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Payment
=====================================


Show payment method
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Use this line to show payment method

.. code-block:: java

	//server_id: game server id
	//role_id: param from game
	//platform: "android"
	//level: character level
	//product_id:  product ID item 
	// if product_id == "" get all productID
	// if product_id = "com.gtv.xxx" get one Item product
	// extinfo : extra info
	GTVSDK.getInstance().payment(server_id, role_id, platform, level,product_id,extinfo, new GtvPaymentCallback() {
      @Override
      public void onFailure() {
         
      }

      public void onFinish(GtvPaymentResponse response) {
         
      }
   });
	
Show payment Gpoint method
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Use this line to show payment Gpoint method

.. code-block:: java

	//server_id: game server id
	//role_id: param from game	
	//product_id:  product ID item 
	
	GTVSDK.getInstance().paymentLocalWithGpoint(server_id, role_id,product_id, new GtvPaymentGpointCallback() {
      @Override
      public void onFailure() {
         
      }

      public void onFinish(GtvPaymentGpointResponse response) {
         
      }
   });