.. index documentation master file, created by
   sphinx-quickstart on Thu Mar 14 14:59:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Authentication
=====================================


Login
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		
Use this line to open authentication webview

.. code-block:: java
   
   GtvSdk.INSTANCE.showLogin(new GtvCallback() {
      @Override
      public void onFinish(GtvPublicResponse gtvResponse) {
         
      }
   });

The GtvPublicResponse instance has these information:

.. code-block:: javascript

   {
      "type": "type", #type of authentication 
      "register": 0, #new register or returning user
      "uuid": "a1b2c3..." #user id
   }

Logout
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Use this line to logout

.. code-block:: java

	GTVSDK.getInstance().logOut();

Get Token
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Use this line to get authentication token

.. code-block:: java

	GTVSDK.getInstance().getAuthToken();

Use this line to get payment token

.. code-block:: java

	GTVSDK.getInstance().getAuthPaymentToken();

Use this line to refresh authentication token

.. code-block:: java

	GTVSDK.getInstance().refreshAuthToken();

Use this line to refresh payment token

.. code-block:: java

	GTVSDK.getInstance().refreshAuthPaymentToken();